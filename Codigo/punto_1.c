#include  <stdio.h>

float func1(float x);
float func2(float x);
float func3(float x);

int main(void){
    float num, funcion_1, funcion_2, funcion_3;
    char opt;
    
    printf("Ingrese una opción (A,B,C)\n");
    scanf("%c",&opt);

    printf("Ingrese un numero decimal\n");
    scanf("%f",&num);

    switch(opt){
        case 'A':
            funcion_1=func1(num);
            printf("\n%f es el cuadradrado de %f.\n",funcion_1,num);
            break;
        case 'B':
            funcion_2=func2(num);
            printf("\n%f es n**2+1000*n si 0<n<5 de %f.\n",funcion_2,num);
            break;
        case 'C':
            funcion_3=func3(num);
            printf("\n%f es %f.\n",funcion_3,num);
            break;
        default:
            printf("Función no valida\n");
            break;
    }
}

float func1(float x){
    return x*x;
}

float func2(float x){
    return (x*x)+1000*x;
}

float func3(float x){
    float n=0.0;
    if(x<=0){
        n=10;
    }

    if(x>0 && x<5){
        n=(x*x)-x+1;
    }

    if(x>=5){
        n=(2*x)-1;
    }
    return n;
}
