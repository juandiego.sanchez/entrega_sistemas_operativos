#include <stdio.h>


void conversoraCelsius(void){
    float conversion=0.0;
    float valor_a_convertir;
    printf("Ingrese el valor a convertir: ");
    scanf("%f",&valor_a_convertir);
    conversion=(valor_a_convertir-32)*(5/9);
    printf("\n%f es la conversion de %f en grados celsius",valor_a_convertir,conversion);
}


void conversoraFahrenheit(void){
    float conversion=0.0;
    float valor_a_convertir;
    float mixta;
    printf("Ingrese el valor a convertir: ");
    scanf("%f",&valor_a_convertir);
    mixta=57.6;// sale de resolver la fraccion mixta 9/5*32 
    conversion=valor_a_convertir+mixta;
    printf("\n%f es la conversion de %f en grados Fahrenheit",valor_a_convertir,conversion);
}


int main(void){
    int opt;
    printf("Ingrese la tarea que desea realizar: \n");
    printf("1) para convertir de fahrenheit a Celsius. \n");
    printf("2) para convertir de Celsius a Fahrenheit. \n");
    scanf("%d",&opt);
    if(opt==1){
        conversoraCelsius();
    }
    if(opt==2){
        conversoraFahrenheit();
    }
    return 0;
}
