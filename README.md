# **Entrega taller N° 1**
___
## **Ejercicio N° 1**
Realice un programa que solicite un numero decimal n y permita elegir al usuario una de las tres funciones f1(n), f2(n),f3(n) para operarlo, muestre el resultado en la pantalla.

![Ejercicio numero 1](/Imagenes/Ejercicio_uno.png)

## Codigo de respuesta
```
#include  <stdio.h>

float func1(float x);
float func2(float x);
float func3(float x);

int main(void){
    float num, funcion_1, funcion_2, funcion_3;
    char opt;
    
    printf("Ingrese una opción (A,B,C)\n");
    scanf("%c",&opt);

    printf("Ingrese un numero decimal\n");
    scanf("%f",&num);

    switch(opt){
        case 'A':
            funcion_1=func1(num);
            printf("\n%f es el cuadradrado de %f.\n",funcion_1,num);
            break;
        case 'B':
            funcion_2=func2(num);
            printf("\n%f es n**2+1000*n si 0<n<5 de %f.\n",funcion_2,num);
            break;
        case 'C':
            funcion_3=func3(num);
            printf("\n%f es %f.\n",funcion_3,num);
            break;
        default:
            printf("Función no valida\n");
            break;
    }
}

float func1(float x){
    return x*x;
}

float func2(float x){
    return (x*x)+1000*x;
}

float func3(float x){
    float n=0.0;
    if(x<=0){
        n=10;
    }

    if(x>0 && x<5){
        n=(x*x)-x+1;
    }

    if(x>=5){
        n=(2*x)-1;
    }
    return n;
}
```
___
## **Ejercicio N° 2**

Escriba un programa que permita convertir de grados Fahrenheit (°F)
a Celsius (°C) cuando el usuario ingrese la letra f y la operacion
contraria cuando el usuario ingrese la letra c. Tenga presente las
ecuaciones descritas a continuacion:

![Ejercicio numero 2](/Imagenes/Ejercicio_dos.png)

## Codigo de respuesta
```
#include <stdio.h>


void conversoraCelsius(void){
    float conversion=0.0;
    float valor_a_convertir;
    printf("Ingrese el valor a convertir: ");
    scanf("%f",&valor_a_convertir);
    conversion=(valor_a_convertir-32)*(5/9);
    printf("\n%f es la conversion de %f en grados celsius",valor_a_convertir,conversion);
}


void conversoraFahrenheit(void){
    float conversion=0.0;
    float valor_a_convertir;
    float mixta;
    printf("Ingrese el valor a convertir: ");
    scanf("%f",&valor_a_convertir);
    mixta=57.6;// sale de resolver la fraccion mixta 9/5*32 
    conversion=valor_a_convertir+mixta;
    printf("\n%f es la conversion de %f en grados Fahrenheit",valor_a_convertir,conversion);
}


int main(void){
    int opt;
    printf("Ingrese la tarea que desea realizar: \n");
    printf("1) para convertir de fahrenheit a Celsius. \n");
    printf("2) para convertir de Celsius a Fahrenheit. \n");
    scanf("%d",&opt);
    if(opt==1){
        conversoraCelsius();
    }
    if(opt==2){
        conversoraFahrenheit();
    }
    return 0;
}
```
___
## **Ejercicio N°3**
Dada la expresión 

![Ejercicio numero 3-a](/Imagenes/Ejercicio_tres_a.png)

donde 

![Ejercicio numero 3-b](/Imagenes/Ejercicio_tres_b.png)

Normalice los datos de salida de los siguientes intervalos:

- [-1,1]
- [1,10]
- [0.5,1]

Para normalizar utilice la siguiente formula:

![Ejercicio numero 3-b](/Imagenes/Ejercicio_tres_c.png)

Donde:

x son los datos a normalizar

xnorm son valores normalizados 
y b conforman el intervalo en el cual se van a normalizar los valores 

si xmax=xmin entonces xnorm=x
a 
```
#include <stdio.h>

float normalize(float x, float xmin, float xmax, float a, float b) {
    return a + ((x - xmin) * (b - a)) / (xmax - xmin);
}

int main() {
    int A[20] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};

    // Intervalo [-1, 1]
    float a1 = -1.0;
    float b1 = 1.0;
    float xmin1 = A[0];
    float xmax1 = A[19];

    // Intervalo [1, 10]
    float a2 = 1.0;
    float b2 = 10.0;
    float xmin2 = A[0];
    float xmax2 = A[19];

    // Intervalo (0.5, 1]
    float a3 = 0.5;
    float b3 = 1.0;
    float xmin3 = A[0];
    float xmax3 = A[19];

    printf("Intervalo [-1, 1]:\n");
    for (int i = 0; i < 20; i++) {
        float x_norm = normalize(A[i], xmin1, xmax1, a1, b1);
        printf("%.2f ", x_norm);
    }

    printf("\n\nIntervalo [1, 10]:\n");
    for (int i = 0; i < 20; i++) {
        float x_norm = normalize(A[i], xmin2, xmax2, a2, b2);
        printf("%.2f ", x_norm);
    }

    printf("\n\nIntervalo (0.5, 1]:\n");
    for (int i = 0; i < 20; i++) {
        float x_norm = normalize(A[i], xmin3, xmax3, a3, b3);
        printf("%.2f ", x_norm);
    }

    return 0;
}
```